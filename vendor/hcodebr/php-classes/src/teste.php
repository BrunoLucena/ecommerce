<?php

//Create a new PHPMailer instance
$mail = new PHPMailer;

// Tell PHPMailer to use SMTP
$mail->isSMTP();

// Enable SMTP debuggin
// O = off (for production use)
// 1 = Cliente messages
// 2 = Cliente and server messages
$mail->SMTPDebug = 2;

// Ask for HTML friendly debug output
$mail->Debugoutput = 'html';

// Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// user
// $mail->Host = gethostbyname('smtp.gmail.com');
// If your network does not support SMTP over IPv6

// Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

// Set the encryption system to use - sll (deprecated) or tls
$mail->SMTPSecure = 'tls';

// Wheter to use SMTP authentication
$mail->SMTPAuth = true;

// Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "cursophp7hcode@gmail.com";

// Passwor to use for SMTP authentication
$mail->Password = "<?senha?>";

// Set whom the message is to be sent from
$mail->setFrom('cursophp7hcode@gmail.com', 'Curso PHP 7');

// Set an alternative reply-to address
// $mail->addReplyTo('replyto@example.com', 'First Last');

// Set whom the message is to be sent to
$mail->addAddress('bruno.lucenac@gmail.com', 'Suporte Hcode');

// Set the subject line
$mail->Subject = 'Testando a classe PHPMailer com Gmail';

// Read an HTML message body from an external file, convert referenced images to embedded,
// convert HTML into a basic plain text alternative body
$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));

// Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';

// Attach an image file
// $mail->addAttachment('images/phpmailer_mini.png');

// Send the message, check for errors
if (!$mail->send()) 
{
	echo "Mailer Error: " . $mail->ErrorInfo;
}
else
{
	echo "Message sent!";
}

?>